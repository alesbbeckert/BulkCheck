Powershell and BASH script with a GUI for mass-checking IP addresses against the AbuseIPDB database. Input a CSV list of IP addresses (one per line) and return a CSV-formatted report with abuse report information, country code, ISP and domain of each IP checked.

You will need an API key from the AbuseIP Database, available for free at (https://www.abuseipdb.com/account/api).

Special thanks to:
AbuseIPDB
PoshGUI
Potatrix

Feature requests and feedback welcome here or at (https://www.admiralsyn-ackbar.com/contact-the-admiralty/).
